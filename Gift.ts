import { Box } from "./Box";

export class Gift implements Box {

    private giftName: String;

    constructor(giftName: String) {
        this.giftName = giftName;
    }

    public unwrap(): void {
        console.log(this.giftName);
    }
}