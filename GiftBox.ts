import { Box } from "./Box";

export class GiftBox implements Box {
    boxes: Array<Box>;

    constructor() {
        this.boxes = [];
    }

    unwrap(): void {
        for (let box of this.boxes) {
            box.unwrap()
        }
    }

    add(box: Box) {
        this.boxes.push(box);
    }

}