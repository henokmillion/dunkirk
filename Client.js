"use strict";
exports.__esModule = true;
var GiftBox_1 = require("./GiftBox");
var Gift_1 = require("./Gift");
var Client = /** @class */ (function () {
    function Client() {
    }
    Client.prototype.main = function () {
        var giftbox = new GiftBox_1.GiftBox();
        var gift1 = new Gift_1.Gift("barby doll");
        var gift2 = new Gift_1.Gift("buzz lightyear");
        var giftbox2 = new GiftBox_1.GiftBox();
        giftbox.add(gift1);
        giftbox2.add(gift2);
        giftbox.add(giftbox2);
        giftbox.unwrap();
    };
    return Client;
}());
exports.Client = Client;
new Client().main();
