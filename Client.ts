import { GiftBox } from "./GiftBox";
import { Gift } from "./Gift";

export class Client {
    main() {
        const giftbox: GiftBox = new GiftBox();
        const gift1: Gift = new Gift("barby doll");
        const gift2: Gift = new Gift("buzz lightyear");
        const giftbox2: GiftBox = new GiftBox();
        giftbox.add(gift1);
        giftbox2.add(gift2);
        giftbox.add(giftbox2);

        giftbox.unwrap()
    }
}

new Client().main()