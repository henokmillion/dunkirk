"use strict";
exports.__esModule = true;
var Gift = /** @class */ (function () {
    function Gift(giftName) {
        this.giftName = giftName;
    }
    Gift.prototype.unwrap = function () {
        console.log(this.giftName);
    };
    return Gift;
}());
exports.Gift = Gift;
