"use strict";
exports.__esModule = true;
var GiftBox = /** @class */ (function () {
    function GiftBox() {
        this.boxes = [];
    }
    GiftBox.prototype.unwrap = function () {
        for (var _i = 0, _a = this.boxes; _i < _a.length; _i++) {
            var box = _a[_i];
            box.unwrap();
        }
    };
    GiftBox.prototype.add = function (box) {
        this.boxes.push(box);
    };
    return GiftBox;
}());
exports.GiftBox = GiftBox;
